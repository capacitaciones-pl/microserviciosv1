package pe.pangolabs.app_gastos.documentation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pe.pangolabs.app_gastos.documentation.swagger.ServiceDefinitionContext;

@RestController
public class ServiceDefinitionController {

    @Autowired
    private ServiceDefinitionContext definitionContext;

    @GetMapping("/service/{servicename}")
    public String getServiceDefinition(@PathVariable("servicename") String serviceName){
        return definitionContext.getSwaggerDefinition(serviceName);
    }
}
