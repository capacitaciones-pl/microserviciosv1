package pe.pangolabs.app_gastos.dt.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import pe.pangolabs.app_gastos.dt.models.DtFilterModel;
import pe.pangolabs.app_gastos.dt.models.DtModel;
import pe.pangolabs.app_gastos.common.entities.DtEntity;
import pe.pangolabs.app_gastos.common.entities.RegistroDtEntity;
import pe.pangolabs.app_gastos.dt.mappers.DtMapperToEntity;
import pe.pangolabs.app_gastos.dt.mappers.DtMapperToModel;
import pe.pangolabs.app_gastos.dt.repositories.DtFilterRepository;
import pe.pangolabs.app_gastos.common.repositories.RegistroDtRepository;

import java.util.Calendar;
import java.util.List;

@Service
public class DtService {

    @Autowired
    private DtFilterRepository dtFilterRepository;

    @Autowired
    private RegistroDtRepository registroDtRepository;

    @Autowired
    private DtMapperToEntity dtMapperToEntity;

    @Autowired
    private DtMapperToModel dtMapperToModel;

    public Page<DtModel> getAll(Specification<DtEntity> dtEntitySpecification,
                                Pageable pageable) {
        Page<DtEntity> dtEntities = dtFilterRepository.findAll(dtEntitySpecification, pageable);
        return dtEntities.map(dtMapperToModel::map);
    }

    public Page<DtFilterModel> getFilter(String codigoDt, List<String> tipoChoferList, List<Long> estadoDtList,
                                         String codigoChofer, String apellidos, String nombres,
                                         Calendar fechaInicio, Calendar fechaFin, Pageable pageable) {
        return dtFilterRepository.filterDt(codigoDt, tipoChoferList, estadoDtList, codigoChofer,
                apellidos, nombres, fechaInicio, fechaFin, pageable);
    }

    public DtModel getDt(String codigoDt, String tipoChofer) {
        return dtFilterRepository.getDt(codigoDt, tipoChofer);
    }

    public void saveDt(DtModel model) {
        Calendar current = Calendar.getInstance();
        // Se guarda el DT
        model.setUserId(1); // Se debería buscar el verdadero usuario mediante un codigo en el modelo
        model.setFecha(current); // fecha actual
        DtEntity dtEntity = dtMapperToEntity.map(model);
        dtEntity = dtFilterRepository.save(dtEntity);

        // Se crea el registro dt para inicializar el estado en "Sin Registro"
        RegistroDtEntity registroDtEntity = new RegistroDtEntity();
        registroDtEntity.setDt(dtEntity);
        registroDtEntity.getUsuario().setId(2); // 2 Usuario SAP
        registroDtEntity.getEstadoDt().setId(1); // 1 Sin Registro
        registroDtEntity.setFecha(current);
        registroDtEntity.setEstadoRegistro(true); // registro activo
        registroDtRepository.save(registroDtEntity);
    }
}
