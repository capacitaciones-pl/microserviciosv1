package pe.pangolabs.app_gastos.dt.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.pangolabs.app_gastos.dt.models.DtFilterModel;
import pe.pangolabs.app_gastos.dt.models.DtModel;
import pe.pangolabs.app_gastos.common.repositories.DtRepository;

import java.util.Calendar;
import java.util.List;

@Repository
public interface DtFilterRepository extends DtRepository {

    /*@Query(
            value = "SELECT d.codigo_dt AS codigoDt, " +
                    "d.clase_dt AS claseDt, " +
                    "u.apellidos AS apellidos, " +
                    "u.nombres AS nombres, " +
                    "u.codigo_chofer AS codigoChofer, " +
                    "d.tipo_chofer AS tipoChofer, " +
                    "d.descripcion AS descripcion, " +
                    "d.fecha AS fecha, " +
                    "e.estado_dt_id AS estadoDtId, " +
                    "e.descripcion AS estadoDescripcion, " +
                    "(SELECT SUM(a.monto) FROM public.anticipo AS a WHERE a.codigo_dt = d.codigo_dt AND a.tipo_chofer = d.tipo_chofer) AS montoAnticipo, " +
                    "(SELECT SUM(g.monto) FROM public.gasto AS g WHERE g.codigo_dt = d.codigo_dt AND g.tipo_chofer = d.tipo_chofer) AS montoGasto " +
                    "FROM public.dt AS d " +
                    "INNER JOIN public.registro_dt AS r ON r.estado_registro = true AND r.codigo_dt = d.codigo_dt AND r.tipo_chofer = d.tipo_chofer " +
                    "INNER JOIN public.estado_dt AS e ON e.estado_dt_id = r.estado_dt_id " +
                    "INNER JOIN public.usuario AS u ON u.usuario_id = d.usuario_id " +
                    "WHERE (NULL IS NULL OR NULL <= d.fecha) AND ('2020-06-17 06:00:00' IS NULL OR d.fecha <= '2020-06-17 06:00:00') " +
                    "AND (cast(:codigoDt as varchar) IS NULL OR d.codigo_dt LIKE cast(:codigoDt as varchar)%) " +
                    "AND (NULL IS NULL OR u.codigo_chofer LIKE '%') " +
                    "AND (NULL IS NULL OR u.nombres LIKE '%') AND (NULL IS NULL OR u.apellidos LIKE '%') " +
                    "AND d.tipo_chofer IN ('T1') " +
                    "AND e.estado_dt_id IN (1, 2) ",
            countQuery = "SELECT COUNT(1) " +
                    "FROM public.dt AS d " +
                    "INNER JOIN public.registro_dt AS r ON r.estado_registro = true AND r.codigo_dt = d.codigo_dt AND r.tipo_chofer = d.tipo_chofer " +
                    "INNER JOIN public.estado_dt AS e ON e.estado_dt_id = r.estado_dt_id " +
                    "INNER JOIN public.usuario AS u ON u.usuario_id = d.usuario_id " +
                    "WHERE (NULL IS NULL OR NULL <= d.fecha) AND ('2020-06-17 06:00:00' IS NULL OR d.fecha <= '2020-06-17 06:00:00') " +
                    "AND (cast(:codigoDt as varchar) IS NULL OR d.codigo_dt LIKE cast(:codigoDt as varchar)%) " +
                    "AND (NULL IS NULL OR u.codigo_chofer LIKE '%') " +
                    "AND (NULL IS NULL OR u.nombres LIKE '%') AND (NULL IS NULL OR u.apellidos LIKE '%') " +
                    "AND d.tipo_chofer IN ('T1') " +
                    "AND e.estado_dt_id IN (1, 2) ",
            nativeQuery = true)
    Page<DtFilterModel> filterDt(@Param("codigoDt") String codigoDt, Pageable pageable);*/

    @Query(
            value = "SELECT new " +
                    "pe.pangolabs.app_gastos.models.DtFilterModel(" +
                    "d.dtId.codigoDt, d.claseDt, u.apellidos, u.nombres, u.codigoChofer, " +
                    "d.dtId.tipoChofer, d.descripcion, d.fecha, e.id, e.descripcion," +
                    "SUM(a.monto), SUM(g.monto)) " +
                    "FROM DtEntity d " +
                    "INNER JOIN d.registroDtList r " +
                    "INNER JOIN r.estadoDt e " +
                    "LEFT JOIN d.anticipoList a " +
                    "LEFT JOIN d.gastoList g " +
                    "INNER JOIN d.usuario u " +
                    "WHERE (:codigoDt IS NULL OR d.dtId.codigoDt LIKE :codigoDt%) " +
                    "AND r.estadoRegistro = true " +
                    "AND (CAST(:fechaInicio AS timestamp) IS NULL OR :fechaInicio <= d.fecha) AND (CAST(:fechaFin AS timestamp) IS NULL OR d.fecha <= :fechaFin) " +
                    "AND ((:tipoChoferList) IS NULL OR d.dtId.tipoChofer IN (:tipoChoferList)) " +
                    "AND (:codigoChofer IS NULL OR u.codigoChofer LIKE :codigoChofer%) " +
                    "AND (LOWER(u.apellidos) LIKE LOWER('%' || :apellidos || '%')) " +
                    "AND (LOWER(u.nombres) LIKE LOWER('%' || :nombres || '%')) " +
                    "AND ((:estadoDtList) IS NULL OR e.id IN (:estadoDtList)) " +
                    "GROUP BY d.dtId.codigoDt, d.claseDt, u.apellidos, u.nombres, u.codigoChofer, " +
                    "d.dtId.tipoChofer, d.descripcion, d.fecha, e.id, e.descripcion"
    )
    Page<DtFilterModel> filterDt(
            @Param("codigoDt") String codigoDt,
            @Param("tipoChoferList") List<String> tipoChoferList,
            @Param("estadoDtList") List<Long> estadoDtList,
            @Param("codigoChofer") String codigoChofer,
            @Param("apellidos") String apellidos,
            @Param("nombres") String nombres,
            @Param("fechaInicio") Calendar fechaInicio,
            @Param("fechaFin") Calendar fechaFin,
            Pageable pageable
    );

    @Query(
            value = "SELECT new " +
                    "pe.pangolabs.app_gastos.models.DtModel(" +
                    "d.dtId.codigoDt, d.dtId.tipoChofer, u.id, u.nombres, u.apellidos, u.codigoChofer, " +
                    "d.claseDt, d.fecha, d.descripcion, e.id, e.descripcion, SUM(a.monto), SUM(g.monto)) " +
                    "FROM DtEntity d " +
                    "INNER JOIN d.registroDtList r " +
                    "INNER JOIN r.estadoDt e " +
                    "LEFT JOIN d.anticipoList a " +
                    "LEFT JOIN d.gastoList g " +
                    "INNER JOIN d.usuario u " +
                    "WHERE d.dtId.codigoDt = :codigoDt AND d.dtId.tipoChofer = :tipoChofer " +
                    "GROUP BY d.dtId.codigoDt, d.dtId.tipoChofer, u.id, u.nombres, u.apellidos, " +
                    "u.codigoChofer, d.claseDt, d.fecha, d.descripcion, e.id, e.descripcion"
    )
    DtModel getDt(
            @Param("codigoDt") String codigoDt,
            @Param("tipoChofer") String tipoChofer
    );
}
