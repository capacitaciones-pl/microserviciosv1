package pe.pangolabs.app_gastos.dt.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Calendar;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtModel {
    private String codigoDt;
    private String tipoChofer;
    private long userId;
    private String nombres;
    private String apellidos;
    private String codigoChofer;
    private String claseDT;
    private Calendar fecha;
    private String descripcion;
    private long estadoId;
    private String descripcionEstado;
    private BigDecimal totalAnticipos;
    private BigDecimal totalGastos;
}
