package pe.pangolabs.app_gastos.dt.mappers;

import org.modelmapper.Converter;
import org.springframework.stereotype.Component;
import pe.pangolabs.app_gastos.dt.models.DtModel;
import pe.pangolabs.app_gastos.common.mappers.ToModel;
import pe.pangolabs.app_gastos.common.entities.AnticipoEntity;
import pe.pangolabs.app_gastos.common.entities.DtEntity;
import pe.pangolabs.app_gastos.common.entities.GastoEntity;
import pe.pangolabs.app_gastos.common.entities.RegistroDtEntity;

import java.math.BigDecimal;
import java.util.List;

@Component
public class DtMapperToModel extends ToModel<DtEntity, DtModel> {
    @Override
    public DtModel map(DtEntity entity) {
        modelMapper.typeMap(DtEntity.class, DtModel.class)
                .addMapping(src -> src.getDtId().getCodigoDt(), DtModel::setCodigoDt)
                .addMapping(src -> src.getDtId().getTipoChofer(), DtModel::setTipoChofer)
                .addMapping(src -> src.getUsuario().getId(), DtModel::setUserId)
                .addMapping(src -> src.getUsuario().getNombres(), DtModel::setNombres)
                .addMapping(src -> src.getUsuario().getApellidos(), DtModel::setApellidos)
                .addMapping(DtEntity::getDescripcion, DtModel::setDescripcion)
                .addMappings(mapper -> mapper.using(
                        (Converter<List<RegistroDtEntity>, String>) context -> {
                            List<RegistroDtEntity> src = context.getSource();
                            if (src == null)
                                return null;
                            else if (src.size() == 0)
                                return null;
                            else
                                return src.get(0).getEstadoDt().getDescripcion();
                        }).map(DtEntity::getRegistroDtList, DtModel::setDescripcionEstado))
                .addMappings(mapper -> mapper.using(
                        (Converter<List<RegistroDtEntity>, Long>) context -> {
                            List<RegistroDtEntity> src = context.getSource();
                            if (src == null)
                                return null;
                            else if (src.size() == 0)
                                return null;
                            else
                                return src.get(0).getEstadoDt().getId();
                        }).map(DtEntity::getRegistroDtList, DtModel::setEstadoId))
                .addMappings(mapper -> mapper.using(
                        (Converter<List<GastoEntity>, BigDecimal>) context -> {
                            List<GastoEntity> src = context.getSource();
                            if (src == null)
                                return BigDecimal.valueOf(0.00);
                            else if (src.size() == 0)
                                return BigDecimal.valueOf(0.00);
                            else
                                return src.stream().map(GastoEntity::getMonto)
                                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                        }).map(DtEntity::getGastoList, DtModel::setTotalGastos))
                .addMappings(mapper -> mapper.using(
                        (Converter<List<AnticipoEntity>, BigDecimal>) context -> {
                            List<AnticipoEntity> src = context.getSource();
                            if (src == null)
                                return BigDecimal.valueOf(0.00);
                            else if (src.size() == 0)
                                return BigDecimal.valueOf(0.00);
                            else
                                return src.stream().map(AnticipoEntity::getMonto)
                                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                        }).map(DtEntity::getAnticipoList, DtModel::setTotalAnticipos));

        return modelMapper.map(entity, DtModel.class);
    }
}
