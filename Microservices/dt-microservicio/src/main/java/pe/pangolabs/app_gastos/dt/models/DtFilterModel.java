package pe.pangolabs.app_gastos.dt.models;

import lombok.Value;

import java.math.BigDecimal;
import java.util.Calendar;

@Value
public class DtFilterModel {
    String codigoDt;
    String claseDt;
    String apellidos;
    String nombres;
    String codigoChofer;
    String tipoChofer;
    String descripcion;
    Calendar fecha;
    long estadoDtId;
    String estadoDescripcion;
    BigDecimal montoAnticipo;
    BigDecimal montoGasto;
}
