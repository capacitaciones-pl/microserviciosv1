package pe.pangolabs.app_gastos.dt.controllers;

import net.kaczmarzyk.spring.data.jpa.domain.Between;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Joins;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.pangolabs.app_gastos.dt.models.DtFilterModel;
import pe.pangolabs.app_gastos.dt.models.DtModel;
import pe.pangolabs.app_gastos.dt.services.DtService;
import pe.pangolabs.app_gastos.common.entities.DtEntity;

import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api/v1/documentos-transporte")
public class DtController {

    @Autowired
    private DtService service;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Page<DtModel>> getDts(
            @Joins({
                    @Join(path= "registroDtList", alias = "r"),
                    @Join(path= "usuario", alias = "u")
            })
            @And({
                    @Spec(path = "fecha", params = {"despues_de", "antes_de"}, spec = Between.class),
                    @Spec(path = "dtId.codigoDt", params = "codigo_dt", spec = Like.class),
                    @Spec(path = "dtId.tipoChofer", params = "tipo_chofer", spec = In.class),
                    @Spec(path = "r.estadoDt.id", params = "estado_id", spec = In.class),
                    @Spec(path = "r.estadoRegistro", constVal = "true", spec = Equal.class ),
                    @Spec(path = "u.nombres", params = "nombre_chofer", spec = Like.class),
                    @Spec(path = "u.apellidos", params = "apellido_chofer", spec = Like.class),
                    @Spec(path = "u.codigoChofer", params = "codigo_chofer", spec = Like.class),
            }) Specification<DtEntity> dtEntitySpecification,
            Pageable pageable
    ) {
        return ResponseEntity.ok(service.getAll(dtEntitySpecification, pageable));
    }

    @GetMapping(path = "/1", produces = "application/json")
    public ResponseEntity<Page<DtFilterModel>> getDtFilter(
            @RequestParam(name = "codigo_dt", required = false) String codigoDt,
            @RequestParam(name = "tipo_chofer", required = false) List<String> tipoChoferList,
            @RequestParam(name = "estado_dt", required = false) List<Long> estadoDtList,
            @RequestParam(name = "codigo_chofer", required = false) String codigoChofer,
            @RequestParam(name = "apellidos", required = false, defaultValue = "") String apellidos,
            @RequestParam(name = "nombres", required = false, defaultValue = "") String nombres,
            @RequestParam(name = "fecha_inicio", required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
                    Calendar fechaInicio,
            @RequestParam(name = "fecha_fin", required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
                    Calendar fechaFin,
            Pageable pageable
    ) {
        if (fechaFin != null)
            fechaFin.add(Calendar.DAY_OF_MONTH, 1);
        return ResponseEntity.ok(service.getFilter(codigoDt, tipoChoferList, estadoDtList, codigoChofer,
                apellidos, nombres, fechaInicio, fechaFin, pageable));
    }

    @GetMapping(path = "/{codigoDt}/{tipoChofer}", produces = "application/json")
    public ResponseEntity<DtModel> getDt(
            @PathVariable String codigoDt,
            @PathVariable String tipoChofer
    ) {
        return ResponseEntity.ok(service.getDt(codigoDt, tipoChofer));
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity saveDt(@RequestBody DtModel model) {
        service.saveDt(model);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
