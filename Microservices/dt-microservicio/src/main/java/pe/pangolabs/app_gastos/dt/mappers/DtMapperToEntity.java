package pe.pangolabs.app_gastos.dt.mappers;

import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;
import pe.pangolabs.app_gastos.common.mappers.ToEntity;
import pe.pangolabs.app_gastos.dt.models.DtModel;
import pe.pangolabs.app_gastos.common.entities.DtEntity;

@Component
public class DtMapperToEntity extends ToEntity<DtModel, DtEntity> {
    @Override
    public DtEntity map(DtModel model) {
        TypeMap<DtModel, DtEntity> typeMap =
                modelMapper.getTypeMap(DtModel.class, DtEntity.class);
        if (typeMap == null) {
            modelMapper.addMappings(new PropertyMap<DtModel, DtEntity>() {
                @Override
                protected void configure() {
                    map().getDtId().setCodigoDt(source.getCodigoDt());
                    map().getDtId().setTipoChofer(source.getTipoChofer());
                    map().getUsuario().setId(source.getUserId());
                }
            });
        }
        return modelMapper.map(model, DtEntity.class);
    }
}
