package pe.pangolabs.app_gastos.tipo_documento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.pangolabs.app_gastos.common.entities.TipoDocumentoEntity;
import pe.pangolabs.app_gastos.common.repositories.TipoDocumentoRepository;

@RestController
@RequestMapping("/api/v1/tipo-documentos")
public class TipoDocumentoController {
    @Autowired
    TipoDocumentoRepository tipoDocumentoRepository;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Page<TipoDocumentoEntity>> getTipoDocumentos(Pageable pageable) {
        return ResponseEntity.ok(tipoDocumentoRepository.findAll(pageable));
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> postTipoDocumento(@RequestBody TipoDocumentoEntity tipoDocumentoEntity) {
        tipoDocumentoEntity = tipoDocumentoRepository.save(tipoDocumentoEntity);
        return ResponseEntity.ok(tipoDocumentoEntity.getId());
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> putTipoDocumento(@RequestBody TipoDocumentoEntity tipoDocumentoEntity) {
        tipoDocumentoEntity = tipoDocumentoRepository.save(tipoDocumentoEntity);
        return ResponseEntity.ok(tipoDocumentoEntity.getId());
    }
}
