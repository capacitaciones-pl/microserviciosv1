package pe.pangolabs.app_gastos.tipo_gasto;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(generateAPIInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("pe.pangolabs.app_gastos.tipo_gasto.controllers"))
                .paths(PathSelectors.any())
                .build()
                .tags(
                        new Tag("Microservicio Gastos", "Esta API contiene el CRUD para gastos")
                );
    }

    //Api information
    private ApiInfo generateAPIInfo() {
        return new ApiInfo("Swagger Tipo-Gasto-Service",
                "Microservicio para el ingreso de tipo de gastos de viaje en documentos de transporte.",
                "0.0.1-SNAPSHOT",
                "https://pangolabs.pe/",
                getContacts(),"", "", new ArrayList());
    }

    // Developer Contacts
    private Contact getContacts() {
        return new Contact("PangoLabs S.R.L.",
                "https://pangolabs.pe/",
                "walter.bejar@pangolabs.pe");
    }
}
