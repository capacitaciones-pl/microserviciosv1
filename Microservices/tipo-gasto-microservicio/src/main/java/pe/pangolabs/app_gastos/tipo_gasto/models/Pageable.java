package pe.pangolabs.app_gastos.tipo_gasto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pageable<T> {
    private List<T> content = new ArrayList<>();
    private int totalElements;
    private int numberOfElements;
}
