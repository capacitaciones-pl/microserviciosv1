package pe.pangolabs.app_gastos.tipo_gasto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.pangolabs.app_gastos.common.entities.TipoGastoEntity;
import pe.pangolabs.app_gastos.common.repositories.TipoGastoRepository;
import pe.pangolabs.app_gastos.tipo_gasto.services.TipoGastoService;

@RestController
@RequestMapping("/api/v1/tipo-gastos")
public class TipoGastoController {
    @Autowired
    private TipoGastoRepository tipoGastoRepository;

    @Autowired
    private TipoGastoService tipoGastoService;

    @GetMapping(path = "/gastos", produces = "application/json")
    public ResponseEntity<Object> geGastos() {
        return ResponseEntity.ok(tipoGastoService.getGastos());
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<Page<TipoGastoEntity>> getTipoGastos(Pageable pageable) {
        return ResponseEntity.ok(tipoGastoRepository.findAll(pageable));
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> postTipoGasto(@RequestBody TipoGastoEntity tipoGastoEntity) {
        tipoGastoEntity = tipoGastoRepository.save(tipoGastoEntity);
        return ResponseEntity.ok(tipoGastoEntity.getId());
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> putTipoGasto(@RequestBody TipoGastoEntity tipoGastoEntity) {
        tipoGastoEntity = tipoGastoRepository.save(tipoGastoEntity);
        return ResponseEntity.ok(tipoGastoEntity.getId());
    }
}
