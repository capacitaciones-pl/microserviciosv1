package pe.pangolabs.app_gastos.tipo_gasto.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="${pangolabs.services.gasto.value}", path = "${pangolabs.services.gasto.path}", fallback=GastoFallbackService.class)
public interface GastoService {

    @GetMapping(produces = "application/json")
    ResponseEntity<Object> getGastos(
            @RequestParam(name = "codigo_dt") String codigoDt,
            @RequestParam(name = "tipo_chofer") String tipoChofer
    );
}
