package pe.pangolabs.app_gastos.tipo_gasto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.pangolabs.app_gastos.tipo_gasto.feign.GastoService;

@Service
public class TipoGastoService {

    @Autowired
    private GastoService gastoService;

    public Object getGastos() {
        return gastoService.getGastos("8000557249","T1").getBody();
    }
}
