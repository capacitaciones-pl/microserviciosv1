package pe.pangolabs.app_gastos.anticipo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.pangolabs.app_gastos.common.entities.AnticipoEntity;
import pe.pangolabs.app_gastos.common.repositories.AnticipoRepository;

@RestController
@RequestMapping("/api/v1/anticipos")
public class AnticipoController {
    @Autowired
    AnticipoRepository anticipoRepository;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Page<AnticipoEntity>> getAnticipos(Pageable pageable) {
        return ResponseEntity.ok(anticipoRepository.findAll(pageable));
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> postAnticipo(@RequestBody AnticipoEntity anticipoEntity) {
        anticipoEntity = anticipoRepository.save(anticipoEntity);
        return ResponseEntity.ok(anticipoEntity.getId());
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Long> putAnticipo(@RequestBody AnticipoEntity anticipoEntity) {
        anticipoEntity = anticipoRepository.save(anticipoEntity);
        return ResponseEntity.ok(anticipoEntity.getId());
    }
}
