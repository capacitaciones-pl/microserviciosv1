package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.pangolabs.app_gastos.common.entities.TipoGastoEntity;

public interface TipoGastoRepository extends JpaRepository<TipoGastoEntity, Long> {
}
