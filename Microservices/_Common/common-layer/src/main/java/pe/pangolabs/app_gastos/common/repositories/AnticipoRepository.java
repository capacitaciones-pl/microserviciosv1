package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.pangolabs.app_gastos.common.entities.AnticipoEntity;

@Repository
public interface AnticipoRepository extends JpaRepository<AnticipoEntity, Long> {
}
