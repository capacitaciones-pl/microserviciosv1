package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pe.pangolabs.app_gastos.common.entities.DtEntity;
import pe.pangolabs.app_gastos.common.entities.DtId;

@Repository
public interface DtRepository extends JpaRepository<DtEntity, DtId>, JpaSpecificationExecutor<DtEntity> {

}
