package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "usuario")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="usuario_id")
    private long id;

    @Column(name = "nombres", nullable = false)
    private String nombres;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "codigo_chofer")
    private String codigoChofer;

    @Column(name = "password")
    private String password;

    @Column(name = "dispositivo")
    private String dispositivo;
}
