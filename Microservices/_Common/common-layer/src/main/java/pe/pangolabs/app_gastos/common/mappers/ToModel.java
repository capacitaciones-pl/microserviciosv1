package pe.pangolabs.app_gastos.common.mappers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ToModel<E, M> {
    @Autowired
    protected ModelMapper modelMapper;

    protected abstract M map(E entity);
}
