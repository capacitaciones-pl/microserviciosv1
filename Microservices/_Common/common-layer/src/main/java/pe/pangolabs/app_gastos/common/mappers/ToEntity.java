package pe.pangolabs.app_gastos.common.mappers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ToEntity<M, E> {
    @Autowired
    protected ModelMapper modelMapper;

    protected abstract E map(M model);
}
