package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.pangolabs.app_gastos.common.entities.TraceEntity;

import java.util.List;

@Repository
public interface TraceRepository extends JpaRepository<TraceEntity, Long> {

    List<TraceEntity> findByHostname(@Param("hostname") String hostname, Pageable pageable);
}
