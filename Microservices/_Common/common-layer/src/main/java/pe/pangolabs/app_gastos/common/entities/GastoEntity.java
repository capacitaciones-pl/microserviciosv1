package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;

@Entity
@Table(name = "gasto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GastoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gasto_id")
    private long id;

    @ManyToOne
    @JoinColumns(value = {
            @JoinColumn(
                    name = "codigo_dt",
                    referencedColumnName = "codigo_dt"),
            @JoinColumn(
                    name = "tipo_chofer",
                    referencedColumnName = "tipo_chofer")
    }, foreignKey = @ForeignKey(name = "fk_gasto_dt"))
    private DtEntity dt = new DtEntity();

    @ManyToOne
    @JoinColumn(name = "tipo_gasto_id",
            referencedColumnName = "tipo_gasto_id",
            foreignKey = @ForeignKey(name = "fk_gasto_tipo_gasto"))
    private TipoGastoEntity tipoGasto = new TipoGastoEntity();

    @ManyToOne
    @JoinColumn(name = "tipo_documento_id",
            referencedColumnName = "tipo_documento_id",
            foreignKey = @ForeignKey(name = "fk_gasto_tipo_documento"))
    private TipoDocumentoEntity tipoDocumento = new TipoDocumentoEntity();

    @ManyToOne
    @JoinColumn(name = "estado_gasto_id",
            referencedColumnName = "estado_gasto_id",
            foreignKey = @ForeignKey(name = "fk_gasto_estado_gasto"))
    private EstadoGastoEntity estadoGasto = new EstadoGastoEntity();

    @Column(name = "monto", nullable = false, precision = 7, scale = 2)
    private BigDecimal monto;

    @Column(name = "ruc", nullable = false)
    private String ruc;

    @Column(name = "codigo_documento", nullable = false)
    private String codigoDocumento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", nullable = false)
    private Calendar fecha;

    // cambiar a comentarios
    @Column(name = "observaciones", length = 4000)
    private String observaciones;

    @Column(name = "motivo_rechazo", length = 4000)
    private String motivoRechazo;

}
