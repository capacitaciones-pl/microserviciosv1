package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.pangolabs.app_gastos.common.entities.EstadoDtEntity;

public interface EstadoDtRepository extends JpaRepository<EstadoDtEntity, Long> {
}
