package pe.pangolabs.app_gastos.common.entities;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "dt")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtEntity {
    @EmbeddedId
    private DtId dtId = new DtId();

    @ManyToOne
    @JoinColumn(name = "usuario_id",
            referencedColumnName = "usuario_id",
            foreignKey = @ForeignKey(name = "fk_dt_usuario"))
    private UsuarioEntity usuario;

    @OneToMany(mappedBy = "dt")
    private List<RegistroDtEntity> registroDtList = new ArrayList<>();

    @OneToMany(mappedBy = "dt")
    private List<GastoEntity> gastoList = new ArrayList<>();

    @OneToMany(mappedBy = "dt")
    private List<AnticipoEntity> anticipoList = new ArrayList<>();

    @Column(name = "clase_dt", nullable = false, length = 4)
    private String claseDt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", nullable = false)
    private Calendar fecha;

    @Column(name = "descripcion", nullable = false)
    private String descripcion;
}
