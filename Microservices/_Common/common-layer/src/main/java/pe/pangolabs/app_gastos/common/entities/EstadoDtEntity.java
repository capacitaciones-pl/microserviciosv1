package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "estado_dt")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstadoDtEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="estado_dt_id")
    private long id;

    @Column(name = "descripcion", length = 50, nullable = false)
    private String descripcion;

    @OneToMany(mappedBy = "estadoDt")
    private List<RegistroDtEntity> registroDtEntities = new ArrayList<>();
}
