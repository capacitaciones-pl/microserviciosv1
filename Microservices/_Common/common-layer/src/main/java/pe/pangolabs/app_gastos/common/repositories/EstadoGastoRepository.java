package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.pangolabs.app_gastos.common.entities.EstadoGastoEntity;

public interface EstadoGastoRepository extends JpaRepository<EstadoGastoEntity, Long> {
}
