package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;

@Entity
@Table(name = "anticipo")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnticipoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="anticipo_id")
    private long id;

    @ManyToOne
    @JoinColumns(value = {
            @JoinColumn(
                    name = "codigo_dt",
                    referencedColumnName = "codigo_dt"),
            @JoinColumn(
                    name = "tipo_chofer",
                    referencedColumnName = "tipo_chofer")
    }, foreignKey = @ForeignKey(name = "fk_anticipo_dt"))
    private DtEntity dt;

    @Column(name = "monto", nullable = false, precision = 7, scale = 2)
    private BigDecimal monto;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", nullable = false)
    private Calendar fecha;

    @Column(name = "descripcion", length = 250)
    private String descripcion;
}
