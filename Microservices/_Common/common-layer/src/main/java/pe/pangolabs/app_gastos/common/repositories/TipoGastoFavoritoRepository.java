package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.pangolabs.app_gastos.common.entities.TipoGastoFavoritoEntity;
import pe.pangolabs.app_gastos.common.entities.TipoGastoFavoritoId;

public interface TipoGastoFavoritoRepository extends JpaRepository<TipoGastoFavoritoEntity, TipoGastoFavoritoId> {
}
