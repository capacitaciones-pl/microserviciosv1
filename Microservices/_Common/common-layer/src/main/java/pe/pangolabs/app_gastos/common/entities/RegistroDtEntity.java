package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "registro_dt")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistroDtEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="registro_dt_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "usuario_id",
            referencedColumnName = "usuario_id",
            foreignKey = @ForeignKey(name = "fk_registro_dt_usuario"))
    private UsuarioEntity usuario;

    @ManyToOne
    @JoinColumns(value = {
            @JoinColumn(
                    name = "codigo_dt",
                    referencedColumnName = "codigo_dt"),
            @JoinColumn(
                    name = "tipo_chofer",
                    referencedColumnName = "tipo_chofer")
    }, foreignKey = @ForeignKey(name = "fk_registro_dt_dt"))
    private DtEntity dt;

    @ManyToOne
    @JoinColumn(name = "estado_dt_id",
            referencedColumnName = "estado_dt_id",
            foreignKey = @ForeignKey(name = "fk_registro_dt_estado_dt"))
    private EstadoDtEntity estadoDt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", nullable = false)
    private Calendar fecha;

    @Column(name = "estado_registro", nullable = false)
    private Boolean estadoRegistro;
}
