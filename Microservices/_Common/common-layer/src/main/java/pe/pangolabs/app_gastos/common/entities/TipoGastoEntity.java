package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tipo_gasto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipoGastoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="tipo_gasto_id")
    private long id;

    @Column(name = "descripcion", nullable = false, length = 250)
    private String descripcion;
}
