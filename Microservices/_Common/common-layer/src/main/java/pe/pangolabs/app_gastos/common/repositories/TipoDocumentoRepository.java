package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.pangolabs.app_gastos.common.entities.TipoDocumentoEntity;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumentoEntity, Long> {
}
