package pe.pangolabs.app_gastos.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.pangolabs.app_gastos.common.entities.GastoEntity;

public interface GastoRepository extends JpaRepository<GastoEntity, Long> {
}
