package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipoGastoFavoritoId implements Serializable {
    @Column(name = "usuario_id")
    private long userId;

    @Column(name = "tipo_gasto_id")
    private long tipoGastoId;
}
