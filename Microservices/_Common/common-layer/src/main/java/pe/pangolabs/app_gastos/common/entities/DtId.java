package pe.pangolabs.app_gastos.common.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtId implements Serializable {
    @Column(name = "codigo_dt", length = 20)
    private String codigoDt;

    @Column(name = "tipo_chofer", length = 5)
    private String tipoChofer;
}
