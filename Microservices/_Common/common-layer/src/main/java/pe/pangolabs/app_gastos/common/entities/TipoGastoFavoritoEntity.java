package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tipo_gasto_favorito")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipoGastoFavoritoEntity {
    @EmbeddedId
    private TipoGastoFavoritoId tipoGastoFavoritoId;

    @ManyToOne
    @JoinColumn(name = "usuario_id",
            referencedColumnName = "usuario_id",
            insertable = false, updatable = false,
            foreignKey = @ForeignKey(name = "fk_tipo_gasto_favorito_usuario"))
    private UsuarioEntity usuario;

    @ManyToOne
    @JoinColumn(name = "tipo_gasto_id",
            referencedColumnName = "tipo_gasto_id",
            insertable = false, updatable = false,
            foreignKey = @ForeignKey(name = "fk_tipo_gasto_favorito_tipo_gasto"))
    private TipoGastoEntity tipoGasto;

    @Column(name = "prioridad", nullable = false, length = 250)
    private Integer prioridad;
}
