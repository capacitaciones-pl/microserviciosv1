package pe.pangolabs.app_gastos.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "trace")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TraceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="trace_id")
    private long id;

    @Column(name="hostname", length = 100, nullable = false)
    private String hostname;

    //@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp", nullable = false)
    private Instant timestamp;

    @Column(name="trace", nullable = false, length = 2000)
    private String trace;
}
