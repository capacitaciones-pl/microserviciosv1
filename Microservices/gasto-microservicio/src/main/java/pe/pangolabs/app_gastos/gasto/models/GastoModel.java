package pe.pangolabs.app_gastos.gasto.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Calendar;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GastoModel {
    private Long gastoId;
    private Long tipoDocumentoId;
    private String tipodocumento;
    private Long tipoGastoId;
    private String tipoGasto;
    private Long estadoGastoId;
    private String estadoGasto;
    private String ruc;
    private String codigoDocumento;
    private Calendar fecha;
    private BigDecimal monto;
    private String observaciones;
    private String motivoRechazo;
}
