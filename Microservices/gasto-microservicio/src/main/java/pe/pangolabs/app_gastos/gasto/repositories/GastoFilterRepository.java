package pe.pangolabs.app_gastos.gasto.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.pangolabs.app_gastos.gasto.models.GastoGroupedModel;
import pe.pangolabs.app_gastos.gasto.models.GastoModel;
import pe.pangolabs.app_gastos.common.repositories.GastoRepository;

import java.util.Calendar;
import java.util.List;

@Repository
public interface GastoFilterRepository extends GastoRepository {

    @Query(
            value = "SELECT new " +
                    "pe.pangolabs.app_gastos.gasto.models.GastoGroupedModel" +
                    "(t.id, t.descripcion, SUM(g.monto)) " +
                    "FROM GastoEntity g " +
                    "INNER JOIN g.dt d " +
                    "INNER JOIN g.tipoGasto t " +
                    "WHERE d.dtId.codigoDt = :codigoDt " +
                    "AND d.dtId.tipoChofer = :tipoChofer " +
                    "GROUP BY t.id, t.descripcion"
    )
    Page<GastoGroupedModel> filterGastosGrouped(
            @Param("codigoDt") String codigoDt,
            @Param("tipoChofer") String tipoChofer,
            Pageable pageable
    );

    @Query(
            value = "SELECT new " +
                    "pe.pangolabs.app_gastos.gasto.models.GastoModel" +
                    "(g.id, td.id, td.descripcion, tg.id, tg.descripcion, e.id, e.descripcion, " +
                    "g.ruc, g.codigoDocumento, g.fecha, g.monto, g.observaciones, g.motivoRechazo) " +
                    "FROM GastoEntity g " +
                    "INNER JOIN g.dt d " +
                    "INNER JOIN g.tipoGasto tg " +
                    "INNER JOIN g.tipoDocumento td " +
                    "INNER JOIN g.estadoGasto e " +
                    "WHERE d.dtId.codigoDt = :codigoDt " +
                    "AND d.dtId.tipoChofer = :tipoChofer " +
                    "AND (CAST(:fechaInicio AS timestamp) IS NULL OR :fechaInicio <= g.fecha) " +
                    "AND (CAST(:fechaFin AS timestamp) IS NULL OR g.fecha <= :fechaFin) " +
                    "AND ((:tipoGasto) IS NULL OR tg.id IN (:tipoGasto)) " +
                    "AND ((:tipoDocumento) IS NULL OR td.id IN (:tipoDocumento)) " +
                    "AND ((:estadoGasto) IS NULL OR e.id IN (:estadoGasto)) " +
                    "AND (g.ruc LIKE CONCAT('%',:ruc,'%')) " +
                    "AND (LOWER(g.codigoDocumento) LIKE LOWER('%' || :codigoDocumento || '%'))"
    )
    Page<GastoModel> filterGastos(
            @Param("codigoDt") String codigoDt,
            @Param("tipoChofer") String tipoChofer,
            @Param("fechaInicio") Calendar fechaInicio,
            @Param("fechaFin") Calendar fechaFin,
            @Param("tipoGasto") List<Long> tipoGasto,
            @Param("tipoDocumento") List<Long> tipoDocumento,
            @Param("estadoGasto") List<Long> estadoGasto,
            @Param("ruc") String ruc,
            @Param("codigoDocumento") String codigoDocumento,
            Pageable pageable
    );
}
