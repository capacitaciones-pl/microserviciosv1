package pe.pangolabs.app_gastos.gasto.models;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class GastoGroupedModel {
    Long tipoGastoId;
    String descripcion;
    BigDecimal monto;
}
