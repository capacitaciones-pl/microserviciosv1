package pe.pangolabs.app_gastos.gasto;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@ServletComponentScan
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
@EnableAsync
@EntityScan(basePackages = { "pe.pangolabs.app_gastos.common.entities" })
@EnableJpaRepositories(basePackages = { "pe.pangolabs.app_gastos.common.repositories", "pe.pangolabs.app_gastos.gasto.repositories" })
public class GastoMain {

    public static void main(String[] args) {
        SpringApplication.run(GastoMain.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    /*@Bean
    public HttpTraceRepository htttpTraceRepository()
    {
        return new CustomTraceRepository();
    }*/
}
