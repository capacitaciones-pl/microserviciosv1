package pe.pangolabs.app_gastos.gasto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pe.pangolabs.app_gastos.common.entities.GastoEntity;
import pe.pangolabs.app_gastos.gasto.mappers.GastoMapperToEntity;
import pe.pangolabs.app_gastos.gasto.models.GastoGroupedModel;
import pe.pangolabs.app_gastos.gasto.models.GastoModel;
import pe.pangolabs.app_gastos.gasto.repositories.GastoFilterRepository;

import java.util.Calendar;
import java.util.List;

@Service
public class GastoService {

    @Autowired
    GastoFilterRepository gastoFilterRepository;

    @Autowired
    private GastoMapperToEntity gastoMapperToEntity;

    public Page<GastoGroupedModel> getGastosGrouped(String codigoDt, String tipoChofer, Pageable pageable) {
        return gastoFilterRepository
                .filterGastosGrouped(codigoDt, tipoChofer, pageable);
    }

    public Page<GastoModel> getGastos(String codigoDt, String tipoChofer, Calendar fechaInicio,
                                      Calendar fechaFin, List<Long> tipoGasto, List<Long> tipoDocumento,
                                      List<Long> estadoGasto, String ruc, String codigoDocumento,
                                      Pageable pageable) {
        return gastoFilterRepository
                .filterGastos(codigoDt, tipoChofer, fechaInicio, fechaFin, tipoGasto,
                        tipoDocumento, estadoGasto, ruc, codigoDocumento, pageable);
    }

    public void saveGasto(GastoModel model, String codigoDt, String tipoChofer) {
        GastoEntity gastoEntity = gastoMapperToEntity.map(model);
        gastoEntity.getDt().getDtId().setCodigoDt(codigoDt);
        gastoEntity.getDt().getDtId().setTipoChofer(tipoChofer);
        gastoEntity.getEstadoGasto().setId(1); // asumimos que siempre es 1 : Aceptado
        gastoFilterRepository.save(gastoEntity);
    }

    public void updateGasto(long idGasto, GastoModel model) {
        GastoEntity gastoEntity = gastoFilterRepository.getOne(idGasto); // lazy loaded
        GastoEntity gastoEntitytoUpdated = gastoMapperToEntity.map(model);
        gastoEntitytoUpdated.setId(idGasto);
        gastoEntitytoUpdated.setDt(gastoEntity.getDt()); // documento de transporte
        gastoEntitytoUpdated.setEstadoGasto(gastoEntity.getEstadoGasto());
        gastoFilterRepository.save(gastoEntitytoUpdated);
    }
}
