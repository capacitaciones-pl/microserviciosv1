package pe.pangolabs.app_gastos.gasto.controllers;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.pangolabs.app_gastos.gasto.models.GastoModel;
import pe.pangolabs.app_gastos.gasto.services.GastoService;

import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api/v1/gastos")
@Api(tags = { "Microservicio Gastos" })
public class GastoController {

    @Autowired
    private GastoService gastoService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Object> getGastos(
            @RequestParam(name = "codigo_dt") String codigoDt,
            @RequestParam(name = "tipo_chofer") String tipoChofer,
            @RequestParam(name = "grouped", required = false, defaultValue = "false") Boolean grouped,
            @RequestParam(name = "fecha_inicio", required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
                    Calendar fechaInicio,
            @RequestParam(name = "fecha_fin", required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
                    Calendar fechaFin,
            @RequestParam(name = "tipo_gasto", required = false) List<Long> tipoGasto,
            @RequestParam(name = "tipo_documento", required = false) List<Long> tipoDocumento,
            @RequestParam(name = "estado_gasto", required = false) List<Long> estadoGasto,
            @RequestParam(name = "ruc", required = false, defaultValue = "") String ruc,
            @RequestParam(name = "codigo_documento", required = false, defaultValue = "") String codigoDocumento,
            Pageable pageable
    ) {
        if (grouped) {
            return ResponseEntity.ok(gastoService.getGastosGrouped(codigoDt, tipoChofer, pageable));
        }
        return ResponseEntity.ok(gastoService
                .getGastos(codigoDt, tipoChofer, fechaInicio, fechaFin, tipoGasto,
                        tipoDocumento, estadoGasto, ruc, codigoDocumento, pageable));
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity saveGasto(
            @RequestParam(name = "codigo_dt") String codigoDt,
            @RequestParam(name = "tipo_chofer") String tipoChofer,
            @RequestBody GastoModel model
    ) {
        gastoService.saveGasto(model, codigoDt, tipoChofer);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping(path = "/{idGasto}", consumes = "application/json", produces = "application/json")
    public ResponseEntity updateGasto(
            @PathVariable long idGasto,
            @RequestBody GastoModel model
    ) {
        gastoService.updateGasto(idGasto, model);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
