package pe.pangolabs.app_gastos.gasto.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import pe.pangolabs.app_gastos.common.entities.TraceEntity;
import pe.pangolabs.app_gastos.common.repositories.TraceRepository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomTraceRepository implements HttpTraceRepository {

    @Value("${spring.application.name}")
    private String host;

    @Autowired
    private TraceRepository traceRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @Override
    public List<HttpTrace> findAll() {
        List<TraceEntity> traceEntityList =
                traceRepository.findByHostname(host,
                        PageRequest.of(0, 200, Sort.by(Sort.Direction.DESC, "timestamp")));

        List<HttpTrace> httpTraceList = new ArrayList<>();

        for (TraceEntity traceEntity : traceEntityList)
            httpTraceList.add(objectMapper.readValue(traceEntity.getTrace(), HttpTrace.class));

        return httpTraceList;
    }

    @SneakyThrows
    @Override
    @Async
    public void add(HttpTrace trace) {
        if (trace.getRequest().getUri().getPath().contains("actuator"))
            return;

        TraceEntity traceEntity = new TraceEntity();
        traceEntity.setHostname(host);
        traceEntity.setTrace(objectMapper.writeValueAsString(trace));
        traceEntity.setTimestamp(trace.getTimestamp());
        traceRepository.save(traceEntity);
    }
}
