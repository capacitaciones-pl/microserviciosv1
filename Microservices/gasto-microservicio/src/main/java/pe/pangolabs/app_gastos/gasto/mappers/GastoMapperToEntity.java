package pe.pangolabs.app_gastos.gasto.mappers;

import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;
import pe.pangolabs.app_gastos.common.mappers.ToEntity;
import pe.pangolabs.app_gastos.gasto.models.GastoModel;
import pe.pangolabs.app_gastos.common.entities.GastoEntity;

@Component
public class GastoMapperToEntity extends ToEntity<GastoModel, GastoEntity> {
    @Override
    public GastoEntity map(GastoModel model) {
        TypeMap<GastoModel, GastoEntity> typeMap =
                modelMapper.getTypeMap(GastoModel.class, GastoEntity.class);

        if (typeMap == null) {
            modelMapper.addMappings(new PropertyMap<GastoModel, GastoEntity>() {
                @Override
                protected void configure() {
                    map().getTipoGasto().setId(source.getTipoGastoId());
                    map().getTipoDocumento().setId(source.getTipoDocumentoId());
                }
            });
        }

        return modelMapper.map(model, GastoEntity.class);
    }
}
